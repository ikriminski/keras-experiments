import cPickle
import dataedit2
import difdatagenerator
import pandas
import numpy
from sklearn.preprocessing import MinMaxScaler
#This script generates complete data samples for trial for diference data.
#The data is scaled to be between 0 and 1
raw = dataedit2.genrawobject(-200,0)
difdatagenerator.gendif()
dif = pandas.read_csv('difdata.out', engine='python', header=None)
dif = dif.values
dif = dif.astype('float64')
counter = 10
out = numpy.empty((0,7))
for i in range(len(raw)-11):
	a=dif[i:i+10]
	a = a.astype('float64')
	scaler = MinMaxScaler(feature_range=(-1,1))
	a = scaler.fit_transform(a)
	if i == 0:
		out = a
	else:
		out = numpy.append(out,a,axis=0)
	print(out.shape)
	print(dif[counter].shape)
	out = numpy.append(out, [dif[counter]],axis=0)
	counter = counter+1
numpy.savetxt('samples.data', out, delimiter=',') 

