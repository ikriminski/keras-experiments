import pandas
import numpy
#This script calculates profit from neural network output.
def profit(testY):
	difdata = testY
	testpredict = pandas.read_csv('testpredict.out', engine='python', header=None)
	testframe = testpredict.values
	testframe = testframe.astype('float64')
	testframe = numpy.array(testframe)
	length = len(testframe)
	difdata = numpy.array(difdata[len(difdata)-length:,:])
	profit = 1.0
	maxindexarr = numpy.argmax(numpy.absolute(testframe),axis=1)
#maxindexarr = numpy.random.randint(0, high=7, size=maxindexarr.shape)
	counter = 0
#print(maxindexarr)
	for row in difdata:
		if(testframe[counter, maxindexarr[counter]] > 0):
			profit = profit * (1+row[maxindexarr[counter]])
		else:
			profit = profit / (1+row[maxindexarr[counter]])
		counter=counter+1
	return profit

def best(testY):
	difdata = testY
	testpredict = pandas.read_csv('besttestpredict.out', engine='python', header=None)
	testframe = testpredict.values
	testframe = testframe.astype('float64')
	testframe = numpy.array(testframe)
	length = len(testframe)
	difdata = numpy.array(difdata[len(difdata)-length:,:])
	profit = 1.0
	maxindexarr = numpy.argmax(testframe,axis=1)
#maxindexarr = numpy.random.randint(0, high=7, size=maxindexarr.shape)
	counter = 0
#print(maxindexarr)
	for row in difdata:
		profit = profit * (1+row[maxindexarr[counter]])
		counter=counter+1
	return profit
	
