import numpy 
import matplotlib.pyplot as plt
import pandas
import math
import gc
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Dropout
from keras.layers import Highway
from keras.layers import GRU
from keras.layers import GaussianNoise
from keras.layers import Convolution1D
from keras.layers import MaxPooling1D
from keras.layers import AveragePooling1D
from keras.layers import Flatten
from keras.callbacks import ModelCheckpoint
from keras.models import load_model
from sklearn.preprocessing import MinMaxScaler
from sklearn.metrics import mean_squared_error
import sklearn.preprocessing
import dataedit2
import difdatagenerator
import profitcalc
#this script tests a model
start = -10
finish = 0
offset = 0
numtrials = 3
file = open('biglog.txt', 'a')
file.write('test')
def create_dataset(dataset, look_back=10):
	dataX, dataY = [], []
	for i in range(len(dataset)-look_back+1):
		a = dataset[i:(i+look_back), :]
		dataX.append(a)
		#dataY.append(dataset[i + look_back, :])
	return numpy.array(dataX), numpy.array(dataY)

for i in range(1):
	start = start+offset
	finish = finish + offset
	totalprofit = 0
	maxprofit = 0
	totalbestprofit =0
	profitarr = numpy.array([])
	bestprofitarr = numpy.array([])
	dataedit2.genraw(start,finish)
	difdatagenerator.gendif()
	for j in range(numtrials):
		dataframe = pandas.read_csv('raw.data', engine='python', header=None)
		dataset = dataframe.values
		dataset = dataset.astype('float64')
#dataset = dataset.T
		numpy.savetxt('pretest2.out', dataset, delimiter=',')  
		scaler = MinMaxScaler(feature_range=(0, 1))
		dataset = scaler.fit_transform(dataset)
		train_size = int(len(dataset))
		print('trainsize:' + str(train_size))
		test_size = len(dataset) - train_size
		train, test = dataset[0:train_size,:], dataset[train_size:len(dataset),:]
		print(train)
		look_back = 10
		trainX, trainY = create_dataset(train, look_back)
		print(len(trainX))
		#trainX = numpy.delete(trainX, (0), axis=0)
		#train_size = train_size-1;
		yframe = pandas.read_csv('difdata.out', engine='python', header=None)
		ydata = yframe.values
		ydata = ydata.astype('float64')
		ydata = ydata.T
		scaler = MinMaxScaler(feature_range=(-1, 1))
		ydata = scaler.fit_transform(ydata)
		ydata = ydata.T

#exit()
		trainY = ydata[11:train_size,:]
		testX, testY = create_dataset(test, look_back)
		testY = ydata[train_size+4:len(dataset),:]
		model = load_model('0_0max.dat')
		print(trainX)
		trainPredict = model.predict(trainX)
		numpy.savetxt('predict.out', trainPredict, delimiter=',') 
		testPredict = model.predict(trainX)
		numpy.savetxt('testpredict.out', testPredict, delimiter=',') 

		profit = profitcalc.profit()
		
		profitarr = numpy.append(profitarr,[profit])
		totalprofit = totalprofit + profit  
		gc.collect
	avgprofit = totalprofit/numtrials
	out = 'Start:' + str(start) +' Finish:' + str(finish) + ' avgprofit:' + str(avgprofit)  +'\n' + str(profitarr) + '\n'
	print(out)
	file.write(out)
	file.flush()




file.close()
