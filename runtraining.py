import numpy 
import matplotlib.pyplot as plt
import pandas
import math
import gc
from keras.models import Sequential
from keras.layers import Dense
from keras.layers import LSTM
from keras.layers import Dropout
from keras.layers import Highway
from keras.layers import GRU
from keras.layers import GaussianNoise
from keras.layers import Convolution1D
from keras.layers import MaxPooling1D
from keras.layers import AveragePooling1D
from keras.layers import Flatten
from keras.callbacks import ModelCheckpoint
from keras.models import load_model
from sklearn.preprocessing import MinMaxScaler
from sklearn.preprocessing import Normalizer
from sklearn.metrics import mean_squared_error
import sklearn.preprocessing
import dataedit2
import difdatagenerator
import profitcalc
#This script runs a training session

#Some training parameters
start = 0
finish = -160
offset = -20

numtrials = 1

file = open('biglog.txt', 'a')
def create_dataset(dataset, look_back=10):
	dataX, dataY = [], []
	for i in range(len(dataset)/11-1):
		a = dataset[i*11:(i*11+look_back), :]
		dataX.append(a)
		dataY.append(dataset[(i+1)*11-1, :])
	return numpy.array(dataX), numpy.array(dataY)

for i in range(1):
	
	start = start+offset
	finish = finish + offset
	totalprofit = 0
	maxprofit = 0
	totalbestprofit =0
	profitarr = numpy.array([])
	bestprofitarr = numpy.array([])
	for j in range(numtrials):
		#load dataset
		dataframe = pandas.read_csv('samples.data', engine='python', header=None)
		dataset = dataframe.values
		dataset = dataset.astype('float64')
		dataset = dataset[finish*11:start*11,:]
		# split into train and test sets
		train_size = int((len(dataset)/11) * 0.85)
		test_size = len(dataset) - train_size
		train, test = dataset[0:train_size*11,:], dataset[train_size*11:len(dataset),:]
		look_back = 10
		trainX, trainY = create_dataset(train, look_back)
		print(trainX[1])
		print(trainY[1])
		#exit()

		#exit()
	#	counter = 0
	#	for row in ydata:
	#		for k in range(len(row)):
	#			if k != maxindexarr[counter]:
	#				row[k] = 0
	#		counter = counter +1
	#	print(ydata)
		testX, testY = create_dataset(test, look_back)

		checkpoint = ModelCheckpoint('bestmodel.dat', monitor='loss', verbose=1, save_best_only=True, save_weights_only=False, mode='auto', period=1)
		model = Sequential()
		model.add(Convolution1D(1024, 2, border_mode='valid',input_shape=(10,7),activation='tanh'))	
		model.add(Dropout(0.3))
		print(model.output_shape)
		model.add(Convolution1D(512, 2, border_mode='valid',activation='tanh'))
		model.add(MaxPooling1D(pool_length=2, stride=1, border_mode='valid'))
		print(model.output_shape)
		model.add(Dropout(0.3))
		print(model.output_shape)
		model.add(Convolution1D(256, 2, border_mode='valid',activation='tanh'))
		model.add(Dropout(0.3))
		print(model.output_shape)
		model.add(MaxPooling1D(pool_length=2, stride=1, border_mode='valid'))
		print(model.output_shape)
		model.add(Flatten())
		#model.add(GaussianNoise(0.05))
		model.add(Dropout(0.3))
		model.add(Dense(1100,activation='tanh'))
		model.add(Dropout(0.3))
		model.add(Dense(700,activation='tanh'))
		model.add(Dropout(0.3))
		model.add(Dense(400,activation='tanh'))
		model.add(Dropout(0.3))
		model.add(Dense(200,activation='tanh'))
		model.add(Dropout(0.3))
		model.add(Dense(100,activation='tanh'))
		model.add(Dropout(0.3))
		model.add(Dense(7, activation ='tanh'))
		model.compile(loss='mean_squared_error', optimizer='adam')
		
		#model.add(GRU(150, input_dim=7))
		for k in range(500):
			history = model.fit(trainX, trainY, nb_epoch=1, batch_size=10, verbose=2, callbacks=[checkpoint])
		#model.save('model.dat')
			#make predictions
		
			trainPredict = model.predict(trainX)
			numpy.savetxt('predict.out', trainPredict, delimiter=',') 
			testPredict = model.predict(testX)
			numpy.savetxt('testpredict.out', testPredict, delimiter=',') 

			profit = profitcalc.profit(testY)
			if profit > maxprofit:
				model.save(str(i) + '_' +str(j) +'max.dat')
				maxprofit = profit
			#model = load_model('bestmodel.dat')
			testPredict = model.predict(testX)
			numpy.savetxt('besttestpredict.out', testPredict, delimiter=',')
			bestprofit = profitcalc.best(testY)		
			profitarr = numpy.append(profitarr,[profit])
			bestprofitarr = numpy.append(bestprofitarr,[bestprofit])
			totalprofit = totalprofit + profit  
			totalbestprofit = totalbestprofit + bestprofit
		gc.collect
	avgprofit = totalprofit/numtrials
	avgbestprofit = totalbestprofit/numtrials
	out = 'Start:' + str(start) +' Finish:' + str(finish) + ' avgprofit:' + str(avgprofit) + ' avgbestprofit:' + str(avgbestprofit) +'\n' + str(profitarr) + '\n' + str(bestprofitarr) + '\n'
	print(out)
	file.write(out)
	file.flush()
	start = start + offset
	finish = finish + offset




file.close()
plt.plot(profitarr)

plt.savefig('plot.png')
