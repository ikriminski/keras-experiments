# README #

Some Keras experimentation for time series prediction.

### How do I get set up? ###

Dowload 7 historical forex data pairs. Make sure that the second column has the exchange rate.
Modify dataedit2.py to match the names of your data files.
Run organizedata.py
You are now ready to train a model by running runtraining.py