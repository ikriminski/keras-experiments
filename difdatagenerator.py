import pandas
import numpy
#This script generates partially preprocessed data.
#It turns raw forex prices into daily percent change.
def gendif():
	difdata = pandas.read_csv('raw.data', engine='python', header=None)
	sub = [1,1,1,1,1,1,1]
	difdata = difdata.values
	difdata = difdata.astype('float64')
	#difdata =difdata.T
	print(difdata)
	print(len(difdata))
	for i in range(len(difdata)):
		if i > 0:
			if i == 1:
				out = numpy.divide(difdata[i],difdata[i-1])
				out = numpy.subtract(out,sub)
				print(out)
			else:	
				tmp = numpy.divide(difdata[i],difdata[i-1])
				tmp = numpy.subtract(tmp,sub)
				out = numpy.vstack((out, tmp))
	numpy.savetxt('difdata.out', out, delimiter=',') 

