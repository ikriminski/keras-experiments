import pandas
import numpy
import csv
#This script generates raw data for later preprocessing
def genraw(start, finish):
	toend = False
	if finish == 0:
		toend =True
	data = pandas.read_csv('forexdata/euraud.txt', engine='python', header=1, usecols=[1])
	data = data.values
	data = data.astype('float64')
	if toend:
		finish=len(data)
	out = numpy.array(data[start:finish])

	data = pandas.read_csv('forexdata/eurcad.txt', engine='python', header=1, usecols=[1])
	data = data.values
	data = data.astype('float64')
	if toend:
		finish=len(data)
	out = numpy.hstack((out,data[start:finish]))
	data = pandas.read_csv('forexdata/eurchf.txt', engine='python', header=1, usecols=[1])
	data = data.values
	data = data.astype('float64')
	if toend:
		finish=len(data)
	out = numpy.hstack((out,data[start:finish]))
	data = pandas.read_csv('forexdata/eurgbp.txt', engine='python', header=1, usecols=[1])
	data = data.values
	data = data.astype('float64')
	if toend:
		finish=len(data)
	out = numpy.hstack((out,data[start:finish]))
	data = pandas.read_csv('forexdata/eurjpy.txt', engine='python', header=1, usecols=[1])
	data = data.values
	data = data.astype('float64')
	if toend:
		finish=len(data)
	out = numpy.hstack((out,data[start:finish]))
	data = pandas.read_csv('forexdata/eurpln.txt', engine='python', header=1, usecols=[1])
	data = data.values
	data = data.astype('float64')
	if toend:
		finish=len(data)
	out = numpy.hstack((out,data[start:finish]))
	data = pandas.read_csv('forexdata/eurusd.txt', engine='python', header=1, usecols=[1])
	data = data.values
	data = data.astype('float64')
	if toend:
		finish=len(data)
	out = numpy.hstack((out,data[start:finish]))
	print(out)
	numpy.savetxt('raw.data', out, delimiter=',')  

def genrawobject(start, finish):
	toend = False
	if finish == 0:
		toend =True
	data = pandas.read_csv('forexdata/euraud.txt', engine='python', header=1, usecols=[1])
	data = data.values
	data = data.astype('float64')
	if toend:
		finish=len(data)
	out = numpy.array(data[start:finish])

	data = pandas.read_csv('forexdata/eurcad.txt', engine='python', header=1, usecols=[1])
	data = data.values
	data = data.astype('float64')
	if toend:
		finish=len(data)
	out = numpy.hstack((out,data[start:finish]))
	data = pandas.read_csv('forexdata/eurchf.txt', engine='python', header=1, usecols=[1])
	data = data.values
	data = data.astype('float64')
	if toend:
		finish=len(data)
	out = numpy.hstack((out,data[start:finish]))
	data = pandas.read_csv('forexdata/eurgbp.txt', engine='python', header=1, usecols=[1])
	data = data.values
	data = data.astype('float64')
	if toend:
		finish=len(data)
	out = numpy.hstack((out,data[start:finish]))
	data = pandas.read_csv('forexdata/eurjpy.txt', engine='python', header=1, usecols=[1])
	data = data.values
	data = data.astype('float64')
	if toend:
		finish=len(data)
	out = numpy.hstack((out,data[start:finish]))
	data = pandas.read_csv('forexdata/eurpln.txt', engine='python', header=1, usecols=[1])
	data = data.values
	data = data.astype('float64')
	if toend:
		finish=len(data)
	out = numpy.hstack((out,data[start:finish]))
	data = pandas.read_csv('forexdata/eurusd.txt', engine='python', header=1, usecols=[1])
	data = data.values
	data = data.astype('float64')
	if toend:
		finish=len(data)
	out = numpy.hstack((out,data[start:finish]))
	
	print(out)
	numpy.savetxt('raw.data', out, delimiter=',')  
	return out

